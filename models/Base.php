<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "base".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property string|null $databegin
 * @property string|null $datedelivery
 */
class Base extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'price'], 'required'],
            [['id', 'price'], 'integer'],
            [['databegin', 'datedelivery'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'databegin' => 'Databegin',
            'datedelivery' => 'Datedelivery',
        ];
    }
}
